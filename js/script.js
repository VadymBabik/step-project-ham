"use strict";

$(function () {
  $(".tabs-content").not(":first").hide();
  $(".our-services-tabs .tabs-title")
    .click(function (e) {
      e.preventDefault();
      $(".our-services-tabs .tabs-title")
        .removeClass("active")
        .eq($(this).index())
        .addClass("active");
      $(".tabs-content").hide().eq($(this).index()).fadeIn();
    })
    .eq(0)
    .addClass("active");
});

//////
$(function () {
  $(".amazing-work-nav li").on("click", function (event) {
    event.preventDefault();
    let category = $(this).attr("data-info");
    $(".amazing-work-nav li").removeClass("active");
    $(this).addClass("active");
    if ($(this).hasClass("loadbtn")) {
      $(".button-load-more").show();
    } else {
      $(".button-load-more").hide();
    }
    let item = $(".amazing-work-content .amazing-content-item");
    item.fadeOut("slow");
    item.each(function () {
      if ($(this).hasClass(category)) {
        $(this).fadeIn(1000);
      }
    });
  });
});
//
$(function () {
  const elements = $(".amazing-content-item.invisible");
  $(elements).hide();

  if ($(elements).length !== 0) {
    $(".button-load-more").show();
  }
  $(".button-load-more").on("click", function () {
    $(".amazing-content-item.invisible:hidden").slice(0, 12).slideDown("slow");

    $(".amazing-content-item.invisible")
      .addClass("all")
      .removeClass("invisible");
    if ($(".amazing-content-item.invisible:hidden").length === 0) {
      $(".button-load-more").remove();
    }
  });
});

/////

function selectCurrentSmall() {
  $($(".reviews__item-img.small.active")[index - 1]).css({
    "border-color": "#18cfab",
    "align-self": "baseline",
  });
}

function deleteSelectCurrentSmall() {
  $($(".reviews__item-img.small.active")[index - 1]).css({
    "border-color": "rgba(31, 218, 181, 0.2)",
    "align-self": "auto",
  });
}

let index = 1;
let measure = 4;
selectCurrentSmall();

let moveNext = function (counter) {
  if (counter > 0) {
    let items = $(".reviews__item");
    let width = items.parent().width();
    let current = $($(items[0]));
    current.animate(
      { marginLeft: `-${width}px` },
      1000 / counter,
      "linear",
      function () {
        current.css({ margin: "0" });
        $(".reviews__slider ul").append(current);
        moveNext(counter - 1);
      }
    );
  }
};

let movePrev = function (counter) {
  if (counter > 0) {
    let items = $(".reviews__item");
    let width = items.parent().width();
    let current = $($(items[items.length - 1])).css({
      "margin-left": `-${width}px`,
    });

    $(".reviews__slider ul").prepend(current);
    current.animate({ marginLeft: `0` }, 1000 / counter, "linear", function () {
      movePrev(counter - 1);
    });
  }
};

$(document).on("click", "#sliderNext", function (e) {
  e.preventDefault();
  moveNext(1);
  deleteSelectCurrentSmall();
  index++;
  if (index > measure) {
    index = 1;
    let smallItems = $(".reviews__item-img.small");
    for (let i = 0; i < 4; i++) {
      $(smallItems[i]).removeClass("active");
      $(".reviews__mini-slider").append(smallItems[i]);
    }

    smallItems = $(".reviews__item-img.small");
    for (let i = 0; i < 4; i++) {
      $(smallItems[i]).addClass("active");
    }
  }
  selectCurrentSmall();
});

$(document).on("click", "#sliderPrev", function (e) {
  e.preventDefault();
  movePrev(1);
  deleteSelectCurrentSmall();
  index--;
  if (index < 1) {
    index = measure;
    let smallItems = $(".reviews__item-img.small");
    for (
      let i = smallItems.length - 1;
      i > smallItems.length - 1 - measure;
      i--
    ) {
      $(smallItems[i]).addClass("active");
      $(".reviews__mini-slider").prepend(smallItems[i]);
    }

    smallItems = $(".reviews__item-img.small");
    for (
      let i = smallItems.length - 1;
      i > smallItems.length - 1 - measure;
      i--
    ) {
      $(smallItems[i]).removeClass("active");
    }
  }
  selectCurrentSmall();
});

$(document).on("click", ".reviews__item-img.small", function (e) {
  e.preventDefault();
  deleteSelectCurrentSmall();
  let attr = $(this).attr("data-info");
  attr = attr > 4 ? attr % 4 : attr;

  if (attr > index) {
    moveNext(attr - index);
    index = attr;
  } else if (attr < index) {
    movePrev(index - attr);
    index = attr;
  }
  selectCurrentSmall();
});

//

$("#container").imagesLoaded(function () {
  // images have loaded$
  $(".grid").masonry({
    // options
    columnWidth: 373,
    itemSelector: ".grid-item ",
    // horizontalOrder: false,
    gutter: 20,
  });
  //
  $(".gallery_min").masonry({
    // options
    columnWidth: 185,
    itemSelector: ".grid-item_2",
    gutter: 3,
  });

  $(".gallery_min_too").masonry({
    // options
    columnWidth: 122.33,
    itemSelector: ".grid-item_3",
    gutter: 3,
  });
});
///

$(function () {
  let item = $(".grid-item");
  item.hide();
  let maxItemPic = 9;
  item.slice(0, maxItemPic).show();

  $(".button-load-gallery").click(function (e) {
    e.preventDefault();

    let spin = $(".spinner");
    spin.removeClass("spin-none");
    setTimeout(function () {
      item.show();
      spin.addClass("spin-none");
      $("#container").imagesLoaded(function () {
        // images have loaded$
        $(".grid").masonry({
          // options
          columnWidth: 373,
          itemSelector: ".grid-item ",
          horizontalOrder: false,
          gutter: 20,
        });
        //
        $(".gallery_min").masonry({
          columnWidth: 185,
          itemSelector: ".grid-item_2",
          gutter: 3,
        });

        $(".gallery_min_too").masonry({
          // options
          columnWidth: 122.33,
          itemSelector: ".grid-item_3",
          gutter: 3,
        });
      });
      spin.addClass("spin-none");
      $(".gallery-btn-loader").fadeOut();
    }, 2000);
  });
});
